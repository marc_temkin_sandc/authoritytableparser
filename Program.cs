﻿using System;
using System.IO;
using OfficeOpenXml;
using System.Linq;
using System.Collections.Generic;

namespace authoritytableparser
{
    class Program
    {
        ExcelWorksheet wsUS = null;
        ExcelPackage package = null;

        static void Main(string[] args)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            string fp = @"C:\Users\marc.temkin\source\repos\AuthorityTableParser\SignatoryDelegationAuthorityTable.xlsx";
            new Program().Process(fp);
            
        }

        private void Process(string fp)
        {
            InitializeWorkbook(fp);
            Dictionary<int, string> JobFamily = GetHeadersByRange(wsUS);
            JobFamily.ToList().ForEach(item => System.Console.WriteLine($"{item.Key}, {item.Value}"));
            CloseWorkbook();
        }
        private void CloseWorkbook()
        {
            if (package != null)    
                package = null;
        }

        private void InitializeWorkbook(string fp)
        {
            ExcelPackage package = new ExcelPackage(new FileInfo(fp));
            wsUS = package.Workbook.Worksheets["S&C-US"];

        }

        private Dictionary<int, string> GetHeadersByRange(ExcelWorksheet ws, string address = "1:1")
        {
            Dictionary<int, string> JobFamily;
            var query1 = (from cell in ws.Cells[address]
                          where cell.Value is string && !String.IsNullOrEmpty(cell.Value.ToString().Trim())
                          select cell);

            JobFamily =
            query1.Select(cell =>
            new { ea = new ExcelAddress(cell.Address), value = cell.Value.ToString().Trim() }).
            ToDictionary(k => k.ea.Start.Column, v => v.value);
            return JobFamily;
        }
    }
}
